###############################################
# calculations and plots
###############################################
#library(data.table)
#library(segmented)

#   # Status from res doc
#       Z= (red2023$b.SM.U1$gt25-mean(red2023$b.SM.U1$gt25))/sd(red2023$b.SM.U1$gt25)
#       Z= red2023$b.SF.U1$gt25
#       plot(red2023$years, Z, type="l")
# 
#       abline(h=median(Z),col="green")
#       abline(h=median(Z)*0.8,col="yellow")
#       abline(h=median(Z)*0.4,col="red")
# 
#   # sustainable catch, simple minded approach
#       Fmsy.catch= data.frame(year=red2023$years,
#       SM.gt22=(red2023$b.SM.U1$gt22 /red2023$q.SM.U1[1]) *
#         red2023$Fmsy.SM.U1[1] * red2023$Fstatus.SM.U1,
#       SF.gt22= (red2023$b.SF.U1$gt22 /red2023$q.SF.U1[1]) *
#         red2023$Fmsy.SF.U1[1] * red2023$Fstatus.SF.U1,
#       SM.gt25= (red2023$b.SM.U1$gt25 /red2023$q.SM.U1[1]) *
#         red2023$Fmsy.SM.U1[1] * red2023$Fstatus.SM.U1,
#       SF.gt25= (red2023$b.SF.U1$gt25 /red2023$q.SF.U1[1]) *
#         red2023$Fmsy.SF.U1[1] * red2023$Fstatus.SF.U1)
# 
#   # Growth from the MSE
#       Fage= red2023$fishery.selectivity$age
#       Sage= red2023$survey.selectivity$age
# 
#       vb= function(age, k, Linf, t0){
#           L= Linf*(1-exp(-k * (age-t0)))
#           L
#       }
# 
#       length.SM= vb(Fage,red2023$k.SM[1],red2023$Linf.SM[1],red2023$t0.SM[1])
#         length.SM.full= vb(9.5,red2023$k.SM[1],red2023$Linf.SM[1],red2023$t0.SM[1])
#       length.SF= vb(Fage,red2023$k.SF[1],red2023$Linf.SF[1],red2023$t0.SF[1])
#         length.SF.full= vb(9.5,red2023$k.SF[1],red2023$Linf.SF[1],red2023$t0.SF[1])
#       #par(mfcol=c(2,1), mar=c(4,4,1,1), omi=c(.1,.3,.1,.3))
#       plot(Fage, length.SM, xlab= "Age", ylab="Length (cm) MSE",cex=0.75, lwd=2, col="black",
#            type="l", xlim=c(0,25))
#       lines(Fage, length.SF, lwd=2, col="red")
#       legend("topleft",lwd=2,col=1:2,lty=1,legend=c("S. mentella","S. fasciatus"), bty="n")
# 
#       #abline(v=9.5,lwd=1,lty=2,col="grey")
#       #abline(h=length.SF.full,lwd=1,lty=2,col="grey")
#       #abline(h=length.SM.full,lwd=1,lty=2,col="grey")
# 
#   # fishery selectivity at length
#       FselATlen= data.frame(len= vb(Fage,red2023$k.SM[1],red2023$Linf.SM[1],red2023$t0.SM[1]),
#                            sel=red2023$fishery.selectivity$selectivity)
#       plot(FselATlen, ylab= "Selectivity, MSE", xlab="Length (cm)",cex=0.75,
#            lwd=2, col="green", type="p",pch=20)
#       #fit logistic
#       fit.FselATlen= nls(sel~1/(1+exp(-(len-alpha)/beta)),data=FselATlen,
#                         start=list(alpha=25,beta=0.5))
#       lines(FselATlen$len, predict(fit.FselATlen), col="green",lwd=2)
# 
#   # survey selectivity at length
#       SselATlen= data.frame(len= vb(Sage,red2023$k.SM[1],red2023$Linf.SM[1],red2023$t0.SM[1]),
#                            sel=red2023$survey.selectivity$selectivity)
#       #plot(SselATlen, ylab= "Survey selectivity Unit 1, MSE", xlab="Length (cm)",cex=0.75,
#       #     lwd=2, col="black", type="p",pch=20)
#       #fit segmented regression
#       lmfit=lm(sel~ len,data=SselATlen)
#       fit.SselATlen= segmented(lmfit)
#       points(SselATlen, col="blue", pch=20,cex=0.75)
#       lines(SselATlen$len, predict(fit.SselATlen), col="blue",lwd=2)
#       legend("bottomright",bty="n",legend=c(paste0("alpha = ",round(coef(fit.FselATlen)[1],1)),
#                                         paste0("beta = ",round(coef(fit.FselATlen)[2],2)),
#                                         paste0("breakpoint = ",round(fit.SselATlen$psi[2],1))))
#       legend("topleft",bty="n",lwd=2,col=c("green","blue"),legend=c("U1 Fishery","U1 Survey"))
# 
#   #population biomass: NatL*WatL/selectivity.survey/q.survey
#     wATl=red2023$a.param.SM.U1*red2023$lf.SM.U1$length^red2023$b.param.SM.U1
#     bmsATl= red2023$lf.SM.U1[,-1] * wATl
#     #apply(tmp,2,sum)/1000
#     selATl= predict(fit.SselATlen,newdata=data.frame(len=red2023$lf.SM.U1$length))
#     selATl[selATl<0]=0
#     pop.bms.at.l= bmsATl/selATl/red2023$q.SM.U1[1]
# 
#   # fishery removals: population.biomass.atL*selectivty.fishery*Fmsy
#     FselATl= predict(fit.FselATlen,newdata=data.frame(len=red2023$lf.SM.U1$length))
#     TAC.at.length= pop.bms.at.l * FselATl * (1-exp(-red2023$Fmsy.SM.U1[1]))
#     TAC.at.length[is.na(TAC.at.length*0)]=0 # remove NA. need to do the * 0 thing as is.finite does not work on data.frames and I need to get NA to use is.na
#     TAC= round(apply(TAC.at.length,2,sum))
#     plot(red2023$years,TAC, xlab="Year",ylab="Cmsy (t)", type="l",lwd=2,)
#     legend("topleft",lwd=2,col=1:2,lty=1,legend=c("S. mentella","S. fasciatus"), bty="n")
# 
# 
# # consider uncertainty
#     #1) resample q
#     #2) resample Fmsy
#     #3) resample a and b length weight parameters
#     #4) add variance to the vb growth parameters (no variance in MSE)
#     #5) get cv on the survey numbers at length
# 

# a function that fits a model to selectivity at length given selectivity at age
# and vb growth parameters. type="segmented" or "simoid". Output is the fitted model.
# It will plot the fitted function over the data if desired. ... are arguments to par
# for plotting.
# it requires that survey.sel.at.age has named columns "age" and "sel"


#' Selectivity at length from selectiviy at age
#'
#' @param sel.at.age The selectivity at age (data.frame with cols "age" and "sel"
#' @param vb.Linf Von Bertalanffy Linfinity (cm)
#' @param vb.k Von Bertalanffy growth rate (/year)
#' @param vb.t0 Von Bertalanffy t0 (age)
#' @param type Fit a selectivity function to sel at length "logistic", "segemented"
#' @param breaks Initial guess for len position of breakpoints (cm)
#' @param plot Create a plot of the selectivity at length and fit (TRUE or FALSE)
#' @param col Colour of points and lines if plotted
#' @param lwd Width of the line if plotted
#' @param ... Other parameters to pass to par for plotting
#'
#' @return
#' @export
#'
#' @examples
sel.at.len= function(sel.at.age, vb.Linf, vb.k, vb.t0, type="sigmoid",breaks=c(10,15),
                     plot=TRUE, col="black",lwd=2, xlab="Length (cm)", ylab="Selectivity", 
                     ...){
  # vb growth function
  vb= function(age, k, Linf, t0){
    L= Linf * (1-exp(-k * (age-t0)))
    L
  }

  # create a data frame with length and selectivity
  sel.data= data.frame(len= vb(sel.at.age$age, vb.k, vb.Linf, vb.t0),
                  sel=sel.at.age$sel)

  if(type=="segmented"){
    #fit segmented regression
    lmfit=lm(sel~ len, data=sel.data)
    #fit.sel= segmented(lmfit)
    fit.sel= segmented(lmfit, seg.Z=~len, psi=list(len=breaks))
  }

  if(type=="sigmoid"){
    fit.sel= nls(sel~1/(1+exp(-(len-alpha)/beta)),data=sel.data,
                 start= list(alpha=20,beta=0.5))
  }

  if (plot){
    plot(sel.data, type="p", xlab=xlab, ylab=ylab, ...)
    lines(sel.data$len, predict(fit.sel), col=col, lwd=lwd)

    if(type=="segmented"){
      #legend("topleft",bty="n",legend=paste0("breakpoint = ",round(fit.sel$psi[2],1)))
    }

    if(type=="sigmoid"){
      legend("topleft",bty="n",legend=c(
             paste0("alpha = ",round(coef(fit.sel)[1],1)),
             paste0("beta = ",round(coef(fit.sel)[2],2))))
    }
  }

  fit.sel
}

# Bumps up the survey numbers at age to population biomass at age by dividing by survey
# selectivity at length, catchability q, and the length-weight relationship. SS is the 
# subsmapling factor that bumps average tow abundance to stock abundance
# It will plot the population biomass over time if desired. ... are arguments to par
# for plotting


#' Calculate the population biomass given a survey q and selectivity
#'
#' @param years A vector of years corresponding to the survey years
#' @param survey.num.at.len Survey abundance at length 
#' @param sel.at.len.fit Fitted selectivity at length object from from sel.at.len
#' @param q Survey catchability scalar
#' @param prop.U1 The proporportion of stock biomass that is in Unit 1
#' @param lw.a Length weight relationship a parameter (W=a*L^b) (cm)
#' @param lw.b Length weight relationship b parameter (W=a*L^b)
#' @param SS Subsampling factor that bumps up average tow lf to stock numbers at length (x10^6)
#' @param minsel minimum selectivity proportion allowed to influence biomass
#' @param plot Plot the bumped-up population biomass over time (TRUE, FALSE)
#' @param ... Other parameters to pass to par for plotting
#' @description
#' Calculates population biomass from survey biomass, q and selectivity at length. minsel is by 
#'       default 0.1 (10%) which means selectivities <10% do not affect the bumped up biomass.
#' 
#' @return
#' @export
#'
#' @examples
pop.biomass= function(years, survey.num.at.len, sel.at.len.fit, q, prop.U1, lw.a,
                      lw.b, SS, plot=TRUE, ...){
    weight.at.len= lw.a * survey.num.at.len$len^lw.b/1000
    biomass.at.len= survey.num.at.len[,-1] * weight.at.len
    sel.at.len= predict(sel.at.len.fit, newdata=data.frame(len=survey.num.at.len$len))
    sel.at.len[sel.at.len<0.1]=0
    is.naninf= function(x){
      out= is.na(x*0)
      out
      }
    pop.bms.at.len= prop.U1 * SS * biomass.at.len / sel.at.len / q[1]
    pop.bms.at.len[is.naninf(pop.bms.at.len)]=0

  if(plot){
     plot(years, apply(pop.bms.at.len,2,sum), xlab= "Year", ylab="Biomass (kt)", ...)
  }
  pop.bms.at.len= cbind(survey.num.at.len[,1],pop.bms.at.len)
  names(pop.bms.at.len)[1]="len"
  pop.bms.at.len
}


# Determines the TAC given fishery selectivity and an F value
# It will plot the TAC over time if desired. ... are arguments to par
# for plotting


#' Calculate the sustainable maximum catch given selectivity of survey and fishery, and q
#'
#' @param years A vector of years corresponding to the survey years
#' @param population.biomass.at.len Bumped-up population biomass at length (result of running pop.biomass)
#' @param fishery.sel.at.len.fit Fitted selectivity at length object from from sel.at.len
#' @param F.val Sustainable maximum F value (/year)
#' @param plot Plot the allowable maximum catch over time (TRUE, FALSE)
#' @param ... Other parameters to pass to par for plotting
#'
#' @return
#' @export
#'
#' @examples
TAC= function(years=1984:2021, population.biomass.at.len, fishery.sel.at.len.fit,
              F.val, plot=TRUE, ...){

  # fishery removals: population.biomass.atL*selectivity.fishery*Fmsy
  fishery.sel.at.len= predict(fishery.sel.at.len.fit,
                              newdata=data.frame(len= population.biomass.at.len$len))
  TAC.at.len= population.biomass.at.len[,-1] * fishery.sel.at.len * (1-exp(-F.val))
  TAC.at.len[is.na(TAC.at.len*0)]= 0 # remove NA. need to do the * 0 thing as is.finite does not work on data.frames and I need to get NA to use is.na
  TAC= round(apply(TAC.at.len,2,sum),3)
  if(plot){
    plot(years,TAC, xlab="Year",ylab="Allowable catch (kt)", ...)
    #legend("topleft",lwd=2,col=1:2,lty=1,legend=c("S. mentella","S. fasciatus"), bty="n")
  }
  TAC
}



#' Depletion of a cohort with F, M and assuming no growth
#'
#' @param B.init Initial biomass of the recruited cohort
#' @param F.rate Fishing mortality rate
#' @param M.rate Natural mortality rate of recruited biomass
#' @param N.years The number of years to forecast
#'
#' @return
#' @export
#'
#' @examples
depletion.F= function(B.init, F.rate=.045, M.rate=0.10, N.years){
  B= B.init
  C= B.init*(1-exp(-F.rate))
  BC= data.frame(B=B,C=C)
  for (i in 2:N.years){
    Z= F.rate + M.rate
    B= max(c(0, B * exp(-Z)))
    C= min(B,(F.rate/Z) * B * (1-exp(-Z))) # catch taken over the year in same way natural works
    BC[i,]= c(B,C)
  }
  BC
}

#' Depletion of a cohort with a constant TAC, M and assuming no growth
#'
#' @param B.init Initial biomass of the recruited cohort
#' @param TAC the annual catch advice
#' @param M.rate Natural mortality rate of recruited biomass
#' @param N.years The number of years to forecast
#'
#' @return
#' @export
#'
#' @examples
depletion.TAC= function(B.init, TAC= 5, M.rate=0.10, N.years){
  B= B.init
  C= TAC
  BC= data.frame(B=B, C=C)
  for (i in 2:N.years){
    B= max(0,(B*exp(-M.rate/2)-C)*exp(-M.rate/2)) # take the TAC halfway through the year instantly while M works symemtrically throughout the year
    C= min(c(B, TAC))
    BC[i,]= c(B,C)
  }
  BC
}


# tmp.F= depletion.F(100, F.rate=0.045, N.years=20)
# tmp.TAC= depletion.TAC(100, TAC=5, N.years=15)
# 
# par(mfcol=c(2,1),mar=c(2,4,1,1),omi=c(.3,.1,.1,.1))
# plot(tmp.F$B,type="l",xlab="", ylab="Biomass",xlim=c(0,20),ylim=c(0,max(tmp.F$B)), lwd=3, col="red")
# lines(tmp.TAC$B,lwd=3,col="blue")
# legend("topright", bty="n", lwd=3, col=c("blue","red"), legend=c("Constant TAC","Constant F"))
# plot(tmp.TAC$C,type="l",xlab="Year", ylab="Catch",xlim=c(0,20),ylim=c(0,max(tmp.TAC$C)), lwd=3, col="blue")
# lines(tmp.F$C,lwd=3,col="red")
# mtext("Year", side=1, outer=TRUE)
# 


#' Method 3 bootstrapped M
#'
#' @param biomass.s1 The first biomass series 
#' @param biomass.s2 The second biomass series
#' @param M.series The natural mortality values to use (vector)
#' @param M.divisor The divisor of M values to use. Default 2 (Froese). It can be a vector.
#' @param N The number of bootstrap samples
#' @param plot Draws a boxplot of the catch
#' @param ... Any argument to boxplot
#' @description
#' Calculates the permissible catch given survey biomass data series of redfish and uses the Froese
#' rule of thumb which states that a safe (sustainable catch) is about M/2. You can choose the M values
#' to use or use a tool like Jason Cope's M calculator.
#' 
#' The biomass used is the geometric mean of the most recent two datao points.
#' 
#' @return
#' It returns a data frame with M, M divisor, Exploitation rate and allowable catch
#' 
#' @export
#'
#' @examples
method3.bootstrap= function(biomass.s1, biomass.s2, M.series, M.divisor=2, N=1000, plot=T, ...){
  resample <- function(x, ...) x[sample.int(length(x), ...)] #sample is dangerous with single values
  M.samples= resample(M.series, size=N, replace=TRUE)
  Divisor.samples= resample(M.divisor, size=N, replace=TRUE)
  exp.rate= 1-(exp(-M.samples/Divisor.samples))
  exploitable.biomass.s1= exp(mean(log(tail(biomass.s1,2)))) #geometric mean of last 2 years of data
  exploitable.biomass.s2= exp(mean(log(tail(biomass.s2,2)))) #geometric mean of last 2 years of data
  catch= data.frame(M=M.samples, divisor=Divisor.samples, exp.rate=exp.rate, removals.s1=NA, removals.s2=NA)
  for (i in 1:N){
    catch$removals.s1[i]= exploitable.biomass.s1 * exp.rate[i]
    catch$removals.s2[i]= exploitable.biomass.s2 * exp.rate[i]
  }
  if(plot){
    boxplot(catch[,4:5], ...)
    stripchart(catch[,4:5], method="jitter", jitter=0.03, pch=20,cex=0.3, col="red", vertical=TRUE, add=TRUE)
  }
  #catch
}

# method3.bootstrap(red2023$b.SM.U1$gt22, red2023$b.SM.U1$gt25 , M.series=M.cope$M.high.growth, M.divisor=2,
#                   N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals (kt)", notch=FALSE, 
#                   main="S. mentella", col="grey", lwd=2, lty=1, border=c("black"))

