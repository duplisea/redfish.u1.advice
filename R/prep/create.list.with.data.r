#setwd("/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/")
#library(readxl)
#library(data.table)
###############################################
# gather all information into a list
###############################################

# create a list with all the data
  red2023= list()

  # survey years
    red2023$years=1984:2023

  # Survey information and survey res doc PA
    # # import abundance and biomass from unit 1 survey (see Senay res doc 2023, Table 5)
      red2023$a.SM.U1= as.data.frame(read_xlsx("data/raw/U1.survey.abundance.xlsx",sheet=1))
      red2023$a.SF.U1= as.data.frame(read_xlsx("data/raw/U1.survey.abundance.xlsx",sheet=2))
      red2023$a.SB.U1= as.data.frame(read_xlsx("data/raw/U1.survey.abundance.xlsx",sheet=3))
      red2023$b.SM.U1= as.data.frame(read_xlsx("data/raw/U1.survey.biomass.xlsx",sheet=1))
      red2023$b.SF.U1= as.data.frame(read_xlsx("data/raw/U1.survey.biomass.xlsx",sheet=2))
      red2023$b.SB.U1= as.data.frame(read_xlsx("data/raw/U1.survey.biomass.xlsx",sheet=3))

    # Length frequency from Unit 1 survey
      red2023$lf.SM.U1= fread("data/raw/Unit1_M_freql.csv")
      names(red2023$lf.SM.U1)=c("length",red2023$years)
      red2023$lf.SF.U1= fread("data/raw/Unit1_F_freql.csv")
      names(red2023$lf.SF.U1)=c("length",red2023$years)
      red2023$lf.SB.U1= fread("data/raw/Unit1_792_freql.csv")
      names(red2023$lf.SB.U1)=c("length",red2023$years)


    # length-weight values (from survey and used in the MSE)
      red2023$a.param.SM.U1= 0.00762
      red2023$a.param.SF.U1= 0.00762
      red2023$a.param.SB.U1= 0.00762
      red2023$b.param.SM.U1= 3.193
      red2023$b.param.SF.U1= 3.193
      red2023$b.param.SB.U1= 3.193

    #subsmapling factor for survey catch as lf is a mean tow
      #red2023$SS.SM= 4.416 #25+
      #red2023$SS.SF= 4.773 #25+
      red2023$SS.SM= 4.935 #all bump up from res doc: surf_mult/surf_trait/10^6
      red2023$SS.SF= 4.935 #all bump up from res doc: surf_mult/surf_trait/10^6
      
    # Assumed natural mortality of redfish (used in the MSE but derived from literature 
    # methods and ecological reasoning
      red2023$M.SM= 0.10
      red2023$M.SF= 0.125
      
    # The assumption of the proportion of Unit 1 + unit 2 biomass that is in Unit 1
      red2023$prop.U1= 0.75
        
  # MSE values
    # catchability for unit 1 survey from the MSE (McAllister 2021, table 5,6) mean, sd
      red2023$q.SM.U1= c(0.666, 0.046)
      red2023$q.SF.U1= c(0.533, 0.034)
      red2023$q.SB.U1= mean(red2023$q.SM.U1[1],red2023$q.SF.U1[1])

    # Fmsy for the species from the MSE (McAllister 2021, table 5,6) mean,sd
      red2023$Fmsy.SM.U1= c(0.041, 0)
      red2023$Fmsy.SF.U1= c(0.094, 0.032)

    # Status as a proportion of Fmsy to apply
      red2023$Fstatus.SM.U1= 1
      red2023$Fstatus.SF.U1= 0.5

    # Fishery selectivity, unit 1 from MSE
      red2023$fishery.selectivity.SM= data.frame(
        age=c(0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10,10.5,11,
              11.5,12,12.5,13,13.5,14,14.5,15,15.5,16,16.5,17,17.5,18,18.5,19,19.5,20,
              20.5,21,21.5,22,22.5,23,23.5,24,24.5,25,25.5,26,26.5,100),
        selectivity=c(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.25,0.5,0.9,0.95,0.98,1,1,1,1,1,1,
                      1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1))
      
      red2023$fishery.selectivity.SF= data.frame(
        age=c(2.41,3.03,3.65,4.37,5.19,5.86,6.68,6.93,7.04,7.14,7.19,7.29,7.40,7.50,
              7.53,7.65,7.76,7.81,7.86,7.91,8.01,8.32,8.68,9.04,9.61,10.22,10.68,
              11.30,11.97,12.95,13.82,15.31,100),
        selectivity=c(0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.03,0.09,0.18,0.26,0.35,
                      0.44,0.51,0.59,0.67,0.73,0.79,0.85,0.89,0.92,0.96,0.98,0.99,1.00,
                      1.00,1.00,1.00,1.00,1.00,1.00,1))

    # Survey selectivity, unit 1 from MSE.
      red2023$survey.selectivity.SM= data.frame(
        age=c(seq(1,3.5,length=10),seq(3.6,30,length=10)),
        selectivity=c(seq(0,1,length=10),seq(1,1,length=10)))

      red2023$survey.selectivity.SF= data.frame(
        age=c(1.00,1.03,1.13,1.18,1.28,1.34,1.36,1.41,1.49,1.54,1.62,1.67,1.77,1.82,
              1.85,1.95,1.98,   1.99,  2.13,2.34,2.52,2.72,3.01,3.54,4.24,4.88,5.45,6.16,7.04,15,
              100),
        selectivity=c(0.00,0.03,0.10,0.17,0.24,0.29,0.33,0.38,0.43,0.48,0.53,0.57,
                      0.64,0.69,0.73,0.79,0.82, 0.821    ,0.85,0.89,0.92,0.96,1.00,1.0,1.0,1.0,
                      1.0,1.0,1.0,1.0,1))

    # VB growth (from McAllister 2021, tables 5,6) mean, sd
      red2023$k.SM= c(0.096,0)
      red2023$Linf.SM= c(45.82,0)
      red2023$t0.SM= c(-0.5,0)
      red2023$k.SF= c(0.106,0)
      red2023$Linf.SF= c(41.238,0)
      red2023$t0.SF= c(-0.5,0)
      
save(red2023,file="data/red2023.rda")
