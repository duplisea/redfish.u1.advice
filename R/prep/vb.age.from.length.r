# Define von Bertalanffy growth parameters. From chatGPT 12 April 2023

Linf <- 42 # asymptotic length (cm)
K <- 0.085 # growth coefficient (per year)
t0 <- -0.15 # theoretical age at length 0 (years)

# Generate simulated length-at-age data
set.seed(123) # for reproducibility
n <- 100 # number of fish
ages <- rnorm(n, mean = 5, sd = 2) # true ages (years)
lengths <- Linf * (1 - exp(-K * (ages - t0))) + rnorm(n, mean = 0, sd = 3) # observed lengths (cm)

# Estimate age-at-length using von Bertalanffy growth equation
ages_est <- (log(1 - (lengths / Linf)) + log(K) - log(-1 * t0)) / (-1 * K)

# Print the first 10 observations
head(data.frame(lengths, ages, ages_est))

plot(ages,lengths)

