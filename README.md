-   [Purpose](#purpose)
-   [Data](#data)
-   [Method 1 selectivity and q corrected
    survey](#method-1-selectivity-and-q-corrected-survey)
    -   [Selectivity at length](#selectivity-at-length)
    -   [Population biomass](#population-biomass)
    -   [TAC](#tac)
-   [Assumptions made by using the MSE
    parameters](#assumptions-made-by-using-the-mse-parameters)
    -   [Why fit selectivity at length, why not just use the empirical
        relationship](#why-fit-selectivity-at-length-why-not-just-use-the-empirical-relationship)
    -   [Main uncertainties](#main-uncertainties)
-   [Method 2: q corrected aggregate survey biomass and
    Fmsy](#method-2-q-corrected-aggregate-survey-biomass-and-fmsy)
-   [old Method 3: no q correction,
    F=M/2](#old-method-3-no-q-correction-fm2)
-   [New method 3](#new-method-3)
    -   [To Do](#to-do)
    -   [High growth, previous cohorts](#high-growth-previous-cohorts)
    -   [Low growth, new cohorts](#low-growth-new-cohorts)
    -   [Present growth (10 Nov 2023)](#present-growth-10-nov-2023)
-   [Method 4: Method 1 but with F=M/2](#method-4-method-1-but-with-fm2)
-   [Method 5: Method 4 but without a bump-up by survey q or proportion
    of biomass in the Unit 1
    area](#method-5-method-4-but-without-a-bump-up-by-survey-q-or-proportion-of-biomass-in-the-unit-1-area)
-   [TAC for all methods by species and quasi-parsimony
    score](#tac-for-all-methods-by-species-and-quasi-parsimony-score)
-   [Summary of methods and 2021
    values](#summary-of-methods-and-2021-values)
-   [Depletion rate of biomass in absence of new
    production](#depletion-rate-of-biomass-in-absence-of-new-production)
-   [Discussion](#discussion)
-   [References](#references)

## Purpose

The goal is to determine a maximum total allowable catch (TAC) for Unit
1 redfish given the lack of a modelled assessment. In 2018 an MSE was
performed on the stock and the base-case model from the MSE provides
some key parameters that can be used to bump up the survey biomass by
selectivity and q to give an estimate of population biomass. The MSE
also provided estimates of Fmsy for *S. mentella* and *S. fasciatus* and
fishery selectivity for each species and these can be used to determine
the sustainable maximum TAC given current estimates of survey population
size.

We cannot use the MSE directly because it is now five years out of date
(as of May 2023). New knowledge from the survey has also revealed that
the redfish in Unit 1 are growing more slowly than they were in 2018
when the MSE was conducted which means the management procedures from
the MSE are based on the previous faster individual growth of redfish.
Therefore, we borrowed from the MSE fitting in places where it is still
considered valid to estimate current redfish population biomass and
applied the Fmsy values from the MSE and applied them to current stock
biomass.

## Data

All pertinent data have been gathered into a list, **red2023**:

-   Survey total biomass over the Unit 1 area by species
-   Survey total abundance over the Unit 1 area by species
-   Survey length frequency for an average tow by species
-   Length-weight relationship (same for both species)
-   Von Bertalanffy growth rate used in the MSE (same for both species)
-   Survey selectivity at age by species from the MSE
-   Fishery selectivity at age by species from the MSE (recent block:
    1994-2016)
-   Subsampling factor that turn abundance in an average tow to
    abundance in the whole stock area
-   Natural mortality rate assumed for each species in the MSE
-   Catchability of the survey by species from the MSE
-   Fmsy by species from the MSE (called Umsy in the MSE)
-   Von Bertalanffy growth parameters used in the MSE (same for both
    species)

## Method 1 selectivity and q corrected survey

1.  Get the length-frequency (LF) distribution from the most recent
    survey

2.  Get the selectivity at age for the survey from the MSE

3.  Calculate length at age from the VB parameters used in the MSE

4.  Determine the relationship between selectivity at length rather than
    selectivity at age

5.  Divide the survey LF by the survey selectivity at length (step 4)

6.  Divide the LF of the selectivity corrected survey (step 5) by the
    catchability for the survey determined by the MSE and multiply by
    the proportion of stock biomass in Unit 1

7.  Determine the weight at length using the well established length
    weight relationship for redfish

8.  Multiply the population abundance at length (step 6) by the weight
    at length. **This now gives us an estimate of the population biomass
    at length in Unit 1**

9.  Get the selectivity at age for the fishery from the MSE

10. Calculate length at age from the VB parameters used in the MSE

11. Determine the relationship between selectivity at length rather than
    selectivity at age for the fishery

12. Multiply the population biomass at length (step 8) by the fishery
    selectivity at length (step 11) to determine the exploitable biomass
    at length by the fishery

13. multiply the exploitable biomass at length (step 12) by the Fmsy
    stock to get a TAC at length

14. Sum the TAC at length over all length classes to get a total TAC

All the above steps are done by species

### Selectivity at length

Selectivity at length needs to be determined from the MSE selectivity at
age and the Von Bertalanffy growth parameters used in the MSE.

#### Survey

Selectivity at age was digitised from the graphs in the MSE (Fig 7, 14).
For fishery selectivity, the recent time block for selectivity was used.

    fitted.s.sm= sel.at.len(sel.at.age=red2023$survey.selectivity.SM,
                vb.Linf=red2023$Linf.SM[1],
                vb.k=red2023$k.SM[1],
                vb.t0=red2023$t0.SM[1],
                type="segmented",
                breaks=15,
                plot=TRUE, col="black",lwd=2,pch=20,
                xlab="Length (cm)",
                ylab="Survey selectivity",
                main= "S. mentella")

![](README_files/figure-markdown_strict/survey.sel.sm-1.png)

For S. fasciatus a segmented regression was used to determine
selectivity at length given its choppiness in the MSE (Figs 7 and 14).
When using a segmented regression, one needs to provide starting guesses
of the breakpoints. The library segmented was used and it can sometimes
be hard to achieve a fit. Eye-balled starting values close to where they
should be by looking at the graph are good. It usually does not matter
if there is one clear breakpoint but for multiple segments it starts to
matter.

    fitted.s.sf= sel.at.len(sel.at.age=red2023$survey.selectivity.SF,
                vb.Linf=red2023$Linf.SF[1],
                vb.k=red2023$k.SF[1],
                vb.t0=red2023$t0.SF[1],
                type="segmented",
                breaks=c(8,17),
                plot=TRUE, col="black",lwd=2,pch=20,
                xlab="Length (cm)",
                ylab="Survey selectivity",
                main= "S. fasciatus")

![](README_files/figure-markdown_strict/survey.sel.sf-1.png)

#### Fishery

Selectivity at age was digitised from the graphs in the MSE (Fig 7, 14).
For fishery selectivity, the recent time block for selectivity was used.

    fitted.f.sm= sel.at.len(sel.at.age=red2023$fishery.selectivity.SM,
                vb.Linf=red2023$Linf.SM[1],
                vb.k=red2023$k.SM[1],
                vb.t0=red2023$t0.SM[1],
                type="sigmoid",
                plot=TRUE,col="black",lwd=2,pch=20,
                xlab="Length (cm)",
                ylab="Fishery selectivity",
                main= "S. mentella")

![](README_files/figure-markdown_strict/fishery.sel.sm-1.png)

    fitted.f.sf= sel.at.len(sel.at.age=red2023$fishery.selectivity.SF,
                vb.Linf=red2023$Linf.SF[1],
                vb.k=red2023$k.SF[1],
                vb.t0=red2023$t0.SF[1],
                type="sigmoid",
                breaks=c(22,23,27),
                plot=TRUE, col="black",lwd=2,pch=20,
                xlab="Length (cm)",
                ylab="Fishery selectivity",
                main= "S. fasciatus")

![](README_files/figure-markdown_strict/fishery.sel.sf-1.png)

### Population biomass

The population biomass by species in the Unit 1 area is determined by
dividing the survey length-frequency distribution by the survey
selectivity and then dividing by the catchability. This needs to be
bumped-up from the average tow to the whole stock area by the
subsampling factor. This can be summed over length to give the total
population biomass.

    biomass.sm= pop.biomass(years=red2023$years,
                survey.num.at.len=red2023$lf.SM.U1,
                sel.at.len.fit=fitted.s.sm,
                q=red2023$q.SM.U1[1],
                prop.U1=red2023$prop.U1,
                lw.a=red2023$a.param.SM.U1,
                lw.b=red2023$b.param.SM.U1,
                SS=red2023$SS.SM,
                plot=TRUE, type="b",lwd=2,pch=20,
                main= "Unit 1 population biomass, S. mentella")

![](README_files/figure-markdown_strict/bms.sm-1.png)

    biomass.sf= pop.biomass(years=red2023$years,
                survey.num.at.len=red2023$lf.SF.U1,
                sel.at.len.fit=fitted.s.sf,
                q=red2023$q.SF.U1[1],
                prop.U1=red2023$prop.U1,
                lw.a=red2023$a.param.SF.U1,
                lw.b=red2023$b.param.SF.U1,
                SS=red2023$SS.SF,
                plot=TRUE, type="b",lwd=2,pch=20,
                main="Unit 1 population biomass, S. fasciatus")

![](README_files/figure-markdown_strict/bms.sf-1.png)

When bumping-up survey length frequency by the survey selectivity from
the MSE, they need to divide the LF by selectivity and the selectivity
is 0 at small sizes. Dividing by zero is inf and therefore we change all
inf to 0. The reality is that it makes no difference to the TAC at the
end because the fishery selects sizes much larger than the inf. It does
make a difference to population biomass estimates though and therefore,
the population biomass estimates apply only to sizes where the survey
selectivity is not 0 from the MSE and in fact, we cut off any
selectivities below 10% to prevent an unrealistic and very uncertain
bump-up in biomass. This about 7 cm for both species, i.e. the biomass
estimate is for fish &gt;7cm.

### TAC

The TAC by species is determined by applying the Fmsy value from the MSE
to the population biomass after it has been multiplied by the fishery
selectivity. The fishery selectivity determines what portion of the
population by size is vulnerable to fishing mortality and it applies the
Fmsy value only to the vulnerable portion.

    TAC.sm= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sm,
            fishery.sel.at.len.fit= fitted.f.sm,
            F.val= red2023$Fmsy.SM.U1[1],
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. mentella")

![](README_files/figure-markdown_strict/TAC.sm-1.png)

    TAC.sf= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sf,
            fishery.sel.at.len.fit= fitted.f.sf,
            F.val= red2023$Fmsy.SF.U1[1],
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. fasciatus")

![](README_files/figure-markdown_strict/TAC.sf-1.png)

    plot(red2023$years,TAC.sm+TAC.sf, lwd=2, pch=20, type="b", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="Combined species")

![](README_files/figure-markdown_strict/TAC.combined-1.png)

## Assumptions made by using the MSE parameters

1.  Selectivity is actually a function of length and not age
2.  Assumes survey and fishery selectivity at length are the same now as
    in 2018
3.  biggie is the proportion of the U1+2 stock biomass that is in Unit 1
4.  Also a big assumption is the Fmsy is determined by maximising yield
    in biomass and therefore has some depedence on the growth rate

### Why fit selectivity at length, why not just use the empirical relationship

We effectively have. By fitting a function, it makes the method generic
and can incorporate new data. If one wanted an almost purely empirical
relationship, they could use a segmented regression with many
breakpoints.

### Main uncertainties

**Include** Fmsy variance. Proportion of the total stock in the Unit 1
area

**Not include** q variance is small and redundant with the proportion in
of stock in U1 selectivity - there is none length weight: well
established & uncertainty for the sake of uncertainty (UFTSOU)

## Method 2: q corrected aggregate survey biomass and Fmsy

Take the survey biomass &gt;25cm and divide by q, multiply by proportion
in Unit 1. The apply Fmsy from from the MSE to it

    SM.TAC.gt22= red2023$Fmsy.SM.U1[1]*red2023$b.SM.U1$gt22/red2023$q.SM.U1[1]
    SM.TAC.gt25= red2023$Fmsy.SM.U1[1]*red2023$b.SM.U1$gt25/red2023$q.SM.U1[1]
    matplot(red2023$years,cbind(SM.TAC.gt22,SM.TAC.gt25),lwd=2, pch=20,        
         col=c("red","blue"),
         lty=1,
         type="p", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="S. mentella")
    lines(red2023$years,SM.TAC.gt22, col="red", lwd=2)
    lines(red2023$years,SM.TAC.gt25, col="blue",lwd=2)
    legend("topleft",col=c("red","blue"),lwd=2,lty=1,pch=20,
           legend=c(">22cm",">25cm"), bty="n")

![](README_files/figure-markdown_strict/TAC.method2.SM-1.png)

    SF.TAC.gt22= red2023$Fmsy.SF.U1[1]*red2023$b.SF.U1$gt22/red2023$q.SF.U1[1]
    SF.TAC.gt25= red2023$Fmsy.SF.U1[1]*red2023$b.SF.U1$gt25/red2023$q.SF.U1[1]
    matplot(red2023$years,cbind(SF.TAC.gt22,SF.TAC.gt25),lwd=2, pch=20,        
         col=c("red","blue"),
         lty=1,
         type="p", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="S. fasciatus")
    lines(red2023$years,SF.TAC.gt22, col="red", lwd=2)
    lines(red2023$years,SF.TAC.gt25, col="blue",lwd=2)
    legend("bottomleft",col=c("red","blue"),lwd=2,lty=1,pch=20,
           legend=c(">22cm",">25cm"), bty="n")

![](README_files/figure-markdown_strict/TAC.method2.SF-1.png)

## old Method 3: no q correction, F=M/2

Take raw survey indices and apply M/2 as a sustainavble explitation
rate.

    SM.TAC.gt22= (1-exp(-red2023$M.SM/2))*red2023$b.SM.U1$gt22
    SM.TAC.gt25= (1-exp(-red2023$M.SM/2))*red2023$b.SM.U1$gt25
    matplot(red2023$years,cbind(SM.TAC.gt22,SM.TAC.gt25),lwd=2, pch=20,        
         col=c("red","blue"),
         lty=1,
         type="p", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="S. mentella")
    lines(red2023$years,SM.TAC.gt22, col="red", lwd=2)
    lines(red2023$years,SM.TAC.gt25, col="blue",lwd=2)
    legend("topleft",col=c("red","blue"),lwd=2,lty=1,pch=20,
           legend=c(">22cm",">25cm"), bty="n")

![](README_files/figure-markdown_strict/TAC.method3.SM-1.png)

    SF.TAC.gt22= (1-exp(-red2023$M.SF/2))*red2023$b.SF.U1$gt22
    SF.TAC.gt25= (1-exp(-red2023$M.SF/2))*red2023$b.SF.U1$gt25
    matplot(red2023$years,cbind(SF.TAC.gt22,SF.TAC.gt25),lwd=2, pch=20,        
         col=c("red","blue"),
         lty=1,
         type="p", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="S. fasciatus")
    lines(red2023$years,SF.TAC.gt22, col="red", lwd=2)
    lines(red2023$years,SF.TAC.gt25, col="blue",lwd=2)
    legend("bottomleft",col=c("red","blue"),lwd=2,lty=1,pch=20,
           legend=c(">22cm",">25cm"), bty="n")

![](README_files/figure-markdown_strict/TAC.method3.SF-1.png)

## New method 3

### To Do

-   plot just &gt;22 cm both species on the same graph
-   English and French plots
-   only 2011 growth parameters
-   back pocket: 1980 growth, &gt;25 cm
-   Y axis label: potential removals / potentiels de capture

------------------------------------------------------------------------

This takes the last two years of the biomass series, calculate the
geometric mean and apply M sampled from a vector of values. The M
divisor can also be put in a vector of values. M might come out of the
various method of cope for instance.

Box plots are produced for the &gt;22cm and &gt;25cm portions of the
population. The small red lines over-layed on the box plot are the
actual values of catch which result from teh different M values.

### High growth, previous cohorts

![](/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Mplots.high.growth.png)

    method3.bootstrap(red2023$b.SM.U1$gt22, red2023$b.SM.U1$gt25 , M.series=M.cope$M.high.growth,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. mentella", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.M.high.growth.sm-1.png)

I have assumed the same growth rate and its relationship to M for S.
fasciatus as S. mentella.

    method3.bootstrap(red2023$b.SF.U1$gt22, red2023$b.SF.U1$gt25 , M.series=M.cope$M.high.growth,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. fasciatus", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.M.high.growth.sf-1.png)

### Low growth, new cohorts

![](/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Mplots.low.growth.png)

    method3.bootstrap(red2023$b.SM.U1$gt22, red2023$b.SM.U1$gt25 , M.series=M.cope$M.low.growth,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. mentella", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.M.low.growth.sm-1.png)

I have assumed the same growth rate and its relationship to M for S.
fasciatus as S. mentella.

    method3.bootstrap(red2023$b.SF.U1$gt22, red2023$b.SF.U1$gt25 , M.series=M.cope$M.low.growth,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. fasciatus", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.M.low.growth.sf-1.png)

Comparing the allowable catch from the two methods produces the perverse
result of allowing more catch when natural mortality is higher. There is
some logic to this in that fisheries are in competition with natural
mortality and they want to take the fish before natural mortality does.
If there were a stock-recruitment relationship in this, there would be
definite limits to this effect but there are none in this presently.

### Present growth (10 Nov 2023)

![](/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Range%20de%20M/Mplots2023-11-10%2018_59_33.png)

    method3.bootstrap(red2023$b.SM.U1$gt22, red2023$b.SM.U1$gt25 , M.series=M.cope$M.10Nov,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. mentella", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.present.growth.sm-1.png)

I have assumed the same growth rate and its relationship to M for S.
fasciatus as S. mentella.

    method3.bootstrap(red2023$b.SF.U1$gt22, red2023$b.SF.U1$gt25 , M.series=M.cope$M.10Nov,
                      M.divisor=2, N=10000, names=c(">22cm",">25cm"), ylab="Permissable removals
                      (kt)", notch=FALSE, main="S. fasciatus", col="grey", lwd=2, lty=1,
                      border=c("black"))

![](README_files/figure-markdown_strict/TAC.method3.M.present.growth.sf-1.png)

blah blah blah

## Method 4: Method 1 but with F=M/2

    TAC.sm= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sm,
            fishery.sel.at.len.fit= fitted.f.sm,
            F.val= red2023$M.SM/2,
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. mentella")

![](README_files/figure-markdown_strict/TAC.method4.sm-1.png)

    TAC.sf= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sf,
            fishery.sel.at.len.fit= fitted.f.sf,
            F.val= red2023$M.SF/2,
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. fasciatus")

![](README_files/figure-markdown_strict/TAC.method4.sf-1.png)

    plot(red2023$years,TAC.sm+TAC.sf, lwd=2, pch=20, type="b", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="Combined species")

![](README_files/figure-markdown_strict/TAC.method4.combined-1.png)

Method 4 still suffers from the need to determine what proportion of
biomass is in Unit 1 vs Unit 2 but does not have the Fmsy issue that
method 1 has.

## Method 5: Method 4 but without a bump-up by survey q or proportion of biomass in the Unit 1 area

    TAC.sm= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sm,
            fishery.sel.at.len.fit= fitted.f.sm,
            F.val= red2023$M.SM/2,
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. mentella")

![](README_files/figure-markdown_strict/TAC.method5.sm-1.png)

    TAC.sf= TAC(years= red2023$years,
            population.biomass.at.len= biomass.sf,
            fishery.sel.at.len.fit= fitted.f.sf,
            F.val= red2023$M.SF/2,
            plot=TRUE,
            lwd=2,
            pch=20, type="b", main="S. fasciatus")

![](README_files/figure-markdown_strict/TAC.method5.sf-1.png)

    plot(red2023$years,TAC.sm+TAC.sf, lwd=2, pch=20, type="b", 
         xlab="Year",
         ylab="Allowable catch (kt)",
         main="Combined species")

![](README_files/figure-markdown_strict/TAC.method5.combined-1.png)

## TAC for all methods by species and quasi-parsimony score

    load("data/all.tacs.rda")
    sm= all.TAC[,-grep("sf|gt22", names(all.TAC))]
    sf= all.TAC[,-grep("sm|gt22", names(all.TAC))]

    # create a simplified matplot function
    mplot= function(data, pch=20, type="b", lty=1, lwd=2, location="topleft", ...){
      matplot(data[,1], data[,-1], pch=pch, type=type, lty=lty, lwd=lwd, ...)
      legend(location,pch=pch, bty="n", lty=lty, lwd=lwd, legend= names(data)[-1],
             col=1:ncol(data[,-1]))
    }

    mplot(sm, type="l", xlab="Year",ylab="TAC", main="S. mentella")

![](README_files/figure-markdown_strict/allplot1-1.png)

    mplot(sf, type="l", xlab="Year",ylab="TAC", main="S. fasciatus", location="topright")

![](README_files/figure-markdown_strict/allplot2-1.png)

## Summary of methods and 2021 values

<table>
<colgroup>
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"></th>
<th style="text-align: center;">Method 1</th>
<th style="text-align: center;">Method 2*</th>
<th style="text-align: center;">Method 3*</th>
<th style="text-align: center;">Method 4</th>
<th style="text-align: center;">Method 5</th>
<th style="text-align: center;">Source</th>
<th style="text-align: center;">Speculation Score</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><strong>Inputs</strong></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;">lf survey</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">Survey</td>
<td style="text-align: center;">1</td>
</tr>
<tr class="odd">
<td style="text-align: center;">survey biomass</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">Survey</td>
<td style="text-align: center;">2</td>
</tr>
<tr class="even">
<td style="text-align: center;">L-W</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">Survey</td>
<td style="text-align: center;">1</td>
</tr>
<tr class="odd">
<td style="text-align: center;">M</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">Literature</td>
<td style="text-align: center;">6</td>
</tr>
<tr class="even">
<td style="text-align: center;">Sel survey</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">MSE</td>
<td style="text-align: center;">6</td>
</tr>
<tr class="odd">
<td style="text-align: center;">q survey</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">MSE</td>
<td style="text-align: center;">8</td>
</tr>
<tr class="even">
<td style="text-align: center;">prop Unit 1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">Speculation</td>
<td style="text-align: center;">10</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Sel fishery</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">MSE</td>
<td style="text-align: center;">5</td>
</tr>
<tr class="even">
<td style="text-align: center;">VB</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">MSE</td>
<td style="text-align: center;">7</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Fmsy</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">0</td>
<td style="text-align: center;">MSE</td>
<td style="text-align: center;">8</td>
</tr>
<tr class="even">
<td style="text-align: center;"><strong>Sustainable TAC
(kt)</strong></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;">S. mentella</td>
<td style="text-align: center;">58.55</td>
<td style="text-align: center;">48.66</td>
<td style="text-align: center;">39.52</td>
<td style="text-align: center;">71.08</td>
<td style="text-align: center;">63.12</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;">S. fasciatus</td>
<td style="text-align: center;">29.92</td>
<td style="text-align: center;">27.34</td>
<td style="text-align: center;">9.69</td>
<td style="text-align: center;">20.21</td>
<td style="text-align: center;">14.36</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;">Combined</td>
<td style="text-align: center;">88.47</td>
<td style="text-align: center;">76.00</td>
<td style="text-align: center;">49.21</td>
<td style="text-align: center;">91.29</td>
<td style="text-align: center;">77.48</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><strong>Information
confidence</strong></td>
<td style="text-align: center;">0.54</td>
<td style="text-align: center;">0.71</td>
<td style="text-align: center;">0.91</td>
<td style="text-align: center;">0.56</td>
<td style="text-align: center;">0.74</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="odd">
<td style="text-align: center;"><strong>Process completeness
score</strong></td>
<td style="text-align: center;">1</td>
<td style="text-align: center;">0.6</td>
<td style="text-align: center;">0.5</td>
<td style="text-align: center;">0.85</td>
<td style="text-align: center;">0.7</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;"><strong>Quasi-parsimony
score</strong></td>
<td style="text-align: center;">0.69</td>
<td style="text-align: center;">0.67</td>
<td style="text-align: center;">0.77</td>
<td style="text-align: center;">0.66</td>
<td style="text-align: center;">0.73</td>
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
</tr>
</tbody>
</table>

\* Method 2 and 3 TAC values show here are only for the &gt;25cm portion
of the biomass

## Depletion rate of biomass in absence of new production

We calculated the rate of depletion of biomass if we assume that no new
production occurred after 2023. Though this is not a realistic
situation, it shows how long a cohort could be expected to contribute to
a fishery during a long period with little or no recruitment. This is a
simple calculation which simply take the fully recruited biomass in the
terminal year and discounts it by the annual mortality as well as the
fishery. The fishery is a modelled in two ways: (1) a fishing mortality
rate applied to the stock continuously throughout the year at the same
time as natural mortality (2) a constant catch (TAC) taken from the
stock instantly halfway through the year while the natural mortality
rate is applied continuously throughout the year.

    Binitial= tail(red2023$b.SB.U1$gt25,1)
    Fconstant= (red2023$Fmsy.SM.U1[1]+red2023$Fmsy.SF.U1[1])/2
    TACconstant= 25
    tmp.F= depletion.F(Binitial, F.rate=Fconstant, M.rate=0.1125, N.years=20)
    tmp.TAC= depletion.TAC(Binitial, TAC=TACconstant, M.rate=0.1125, N.years=20)
    tmp.noF= depletion.F(Binitial, F.rate=0, M.rate=0.1125, N.years=20)

    par(mfcol=c(2,1),mar=c(2,4,1,1),omi=c(.3,.2,.3,.1))
    plot(tmp.F$B,type="l",xlab="", ylab="Present >25cm Biomass",xlim=c(0,20),ylim=c(0,max(tmp.F$B)), lwd=3, col="red")
    lines(tmp.TAC$B,lwd=3,col="blue")
    lines(tmp.noF$B,lwd=3,col="green")
    legend("topright", bty="n", lwd=3, col=c("blue","red","green"), legend=c("Constant TAC","Constant F","No fishing"))
    plot(tmp.TAC$C,type="l",xlab="Year", ylab="Catch",xlim=c(0,20),ylim=c(0,max(tmp.F$C)), lwd=3, col="blue")
    lines(tmp.F$C,lwd=3,col="red")
    legend("topright", bty="n", legend=c(paste0("F = ", round(Fconstant,3)),"M = 0.1125"))
    mtext("Year", side=1, outer=TRUE)
    mtext("Depletion, Sebastes spp.", side=3, outer=TRUE, cex=1.2)

![](README_files/figure-markdown_strict/depletion-1.png)

## Discussion

Methods 3, 4 and 5 depend on Froese 2016 Rule of thumb 1.

The process completeness score is a proportion which describes how many
of the population and fishing process are accounted for in teh
estimation of the TAC. Each of these processes has different levels of
confidence and understanding associated with them however. This is
captured by the speculation score where some of the data inputs and
processes are well understood and measured almost directly (e.g. survey
length frequency) so a speculation score of 1 which means it is not very
speculative. conversely, the proportion of the stock biomass in the Unit
1 area in teh last year is quite speculative especially since we have
issues with stock definition and we have no survey for Unit 2 since
2018, it is therefore given a very high speculation score of 10. We
compute a weighted average the speculation score and the completeness
score where the speculation score is weighted twice that as
completeness, i.e. we want the the data to be a more important
determining factor for the method quality than our good intentions by
including more processes that we may not understand or have data for. We
call the end the result a quasi-parsimony score.

Method 3 and then 5 have the highest quasi-parsimony score. This is
largely because they do not depend on the speculative evalution of what
proportion of the stock biomass is in Unit 1 vs Unit 1+2. In addition
neither method 3 nor 5 depend on q and Fmsy calculated from the MSE
which reduces reliance on important but speculation-laden parameters
from the MSE. Method 3 in fact has no dependence on the 2018 MSE
base-case model at all.

## References

Froese, R., Winker, H., Gascuel, D., Sumaila, U.R. and Pauly, D. 2016.
Minimizing the impact of fishing. Fish and Fisheries 17: pp.785-802.
