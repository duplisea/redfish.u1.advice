library(redfish.U1.advice)

#high growth
load("/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Mparms.high.growth.DMP")

#low growth
load("/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Mparms.low.growth.DMP")

# 10 November 2023 growth agree with Caroline
load("/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/extra/Range de M/M_parms_values_byage_outFri Nov 10 2023 065622 PM.DMP")

For each import of the M object above you need run these chunks of code and save them all to the object M.cope
  # organise data and plot the distribution
  M= as.vector(na.omit(M.out.parms.vals.ages$M_estimates$M))
  M= M[-length(M)] # remove the user input value
  M= M[M>0.04] # keep only values >0.04, lower rates are just not realistic.
  plot(M)
  mean(M)
  hist(M)
  plot(density(M))
  
  
   # M.high.growth= M
   # M.low.growth= M
   # M.10Nov= M
# 
 M.cope=list(M.high.growth=M.high.growth, M.low.growth=M.low.growth, M.10Nov=M.10Nov)
# save(M.cope,file="/home/daniel/working.groups.and.assessments/redfish/caro.F.strategy.April2023/data/M.cope.rda")
     
# fit a log normal using MASS
  fitted= fitdistr(x=M, densfun="lognormal")
  curve(dlnorm(x,coef(fitted)[1],coef(fitted)[2]),0,.5)

# just compute mean and sd of lognormal
  Mmn= mean(log(M))
  Msd= sd(log(M))
  tmp=data.frame(x=seq(0,0.5,length=1000),y=dlnorm(seq(0,0.5,length=1000), Mmn,Msd))
  lines(tmp,col="blue")

# the distributions are not really good fits to data. Just sample it empirically
  